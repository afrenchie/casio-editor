#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

if hasattr(sys,"setdefaultencoding"):
    sys.setdefaultencoding("utf-8")

from tkinter import (Tk, Text, Scrollbar, Menu, messagebox, filedialog, \
    BooleanVar, Checkbutton, Label, Entry, StringVar, Grid, Frame, TclError, \
    Button, W, X, LEFT, END, Listbox, NO, ACTIVE, INSERT, SEL)

from collections import OrderedDict
import os, subprocess, json, string, re, jinja2, platform

MAIN_TITLE_MAX_LEN = 17
MENU_TITLE_MAX_LEN = 26
TEXT_MAX_LEN = 30
TEXT_JUMP_CG20 = [1,15,29,43,57,71,85,99,113,127,141,155]
TEXT_HEIGHT_CG20 = 1
TEXT_JUMP_G35 = [2,8,14,20,26,32,38,44,50,56]
TEXT_HEIGHT_G35 = 2
TEMPLATE_FILE_CG20 = "templates/template_cg20.txt"
TEMPLATE_FILE_G35 = "templates/template_g35.txt"
CG20 = "CG20"
G35 = "G35+"

class Editor():
    def __init__(self, root):
        if platform.system() == "Darwin":
            self.commande = "Control"
            self.cmd = "cmd"
            self.alt = "alt"
            self.command_quit = "Cmd+Q"
        else:
            self.commande = "Ctrl"
            self.cmd = "Ctrl"
            self.alt = "Alt"
            self.command_quit = "Alt+F4"
        self.root = root
        self.TITLE = "Generateur de cours"
        self.file_path = None
        self.set_title()
        self.frame = Frame(self.root)
        self.yscrollbar = Scrollbar(self.frame, orient="vertical")
        self.editor = Text(self.frame,
            yscrollcommand=self.yscrollbar.set,
            bg=None)
        self.editor.pack(side="left", fill="both", expand=1)
        self.editor.config(wrap="word", undo=True, width=40)
        self.editor.focus()

        self.yscrollbar.pack(side="right", fill="y")
        self.yscrollbar.config(command=self.editor.yview)
        self.frame.pack(fill="both", expand=1)

        self.l1 = Label(self.frame, text="Attention !", fg="red")
        self.l1.pack()
        self.l2 = Label(self.frame, text="longueur des lignes <= 30",
            anchor=W, justify=LEFT)
        self.l2.pack(fill=X)
        self.l3 = Label(self.frame, text="##... = titre du menu principal",
            anchor=W, justify=LEFT)
        self.l3.pack(fill=X)
        self.l4 = Label(self.frame, text="//... = titre d'un menu",
            anchor=W, justify=LEFT)
        self.l4.pack(fill=X)
        self.l5 = Label(self.frame, text="--... = titre d'un sous menu",
            anchor=W, justify=LEFT)
        self.l5.pack(fill=X)
        self.button_convert = Button(self.frame, text="Convertir",
            command=self.convert)
        self.button_convert.pack()
        self.button_remove_specs_chars = Button(self.frame,
            text="Retirer car. speciaux et ajuster",
            command=self.remove_specs_chars)
        self.remp_sp_chrs = BooleanVar()
        self.remp_sp_chrs.set(False)
        self.remp_sp_chrs_cb = Checkbutton(self.frame,
            text = "Sp chars (cg20)",
            variable = self.remp_sp_chrs)
        self.remp_sp_chrs_cb.pack()


        self.button_remove_specs_chars.pack()
        self.l5 = Label(self.frame, text="Choix du modele :",
            anchor=W, justify=LEFT)
        self.l5.pack(fill=X)
        self.listtype = Listbox(self.frame)
        for item in ["CG20", "G35+"]:
            self.listtype.insert(END, item)
        self.listtype.select_set(0)
        self.listtype.pack()

        #instead of closing the window, execute a function
        root.protocol("WM_DELETE_WINDOW", self.file_quit)

        #create a top level menu
        self.menubar = Menu(self.root)
        #Menu item File
        filemenu = Menu(self.menubar, tearoff=0)
        # tearoff = 0 => can't be seperated from window
        filemenu.add_command(label="Nouveau", underline=1,
            command=self.file_new, accelerator=self.cmd+"+N")
        filemenu.add_command(label="Ouvrir...", underline=1,
            command=self.file_open, accelerator=self.cmd+"+O")
        filemenu.add_command(label="Sauvegarder", underline=1,
            command=self.file_save, accelerator=self.cmd+"+S")
        filemenu.add_command(label="Sauvegarder en...", underline=5,
            command=self.file_save_as, accelerator="Shift+"+self.cmd+"+S")
        filemenu.add_separator()
        filemenu.add_command(label="Convertir", underline=2,
            command=self.convert, accelerator="Shift+"+self.cmd+"+C")
        filemenu.add_command(label="Retirer caractères spéciaux et ajuster",
            underline=2, command=self.remove_specs_chars,
            accelerator=self.cmd+"+D")
        filemenu.add_command(label="Exemple", underline=2,
            command=self.set_example, accelerator="Shift+"+self.cmd+"+E")
        filemenu.add_separator()
        filemenu.add_command(label="Quitter", underline=2,
            command=self.file_quit, accelerator=self.command_quit)
        self.menubar.add_cascade(label="Fichier", underline=0, menu=filemenu)
        # display the menu
        root.config(menu=self.menubar)

    def save_if_modified(self, event=None):
        if False and self.editor.edit_modified():
            response = messagebox.askyesnocancel("Sauvegarder ?",
                "Ce cours a été modifié, voulez vous sauvegarder ?")
            if response:
                result = self.file_save()
                if result == "saved": #saved
                    return True
                else:
                    return None
            else:
                return response #None = cancel/abort, False = no/discard
        else:
            return True

    def file_new(self, event=None):
        result = self.save_if_modified()
        if result != None:
            self.editor.delete(1.0, "end")
            self.editor.edit_modified(False)
            self.editor.edit_reset()
            self.file_path = None
            self.set_title()

    def set_example(self, event=None, filepath=None):
        example ="""##titre menu principal\n//titre menu 1\nHello\nca va\n//titre menu 2\n--titre sous menu 2-1\nBlablabla\nTexte super long qui doit absolument être coupé pour être prit en compte\n--titre sous menu 2-2\nBlablabla\nBlablabla\nBlablabla\nBlablabla\naaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\na\na\na\na\na\na\na\na\na\na\na\na\na\na\na\na\na"""
        self.editor.delete(1.0, "end")
        self.editor.insert(1.0, example)
        self.editor.edit_modified(False)
        self.file_path = filepath

    def file_open(self, event=None, filepath=None):
        result = self.save_if_modified()
        if result != None:
            if filepath == None:
                filepath = filedialog.askopenfilename()
            if filepath != None  and filepath != '':
                with open(filepath, encoding="utf-8") as f:
                    fileContents = f.read()
                self.editor.delete(1.0, "end")
                self.editor.insert(1.0, fileContents)
                self.editor.edit_modified(False)
                self.file_path = filepath

    def file_save(self, event=None):
        if self.file_path == None:
            result = self.file_save_as()
        else:
            result = self.file_save_as(filepath=self.file_path)
        return result

    def file_save_as(self, event=None, filepath=None):
        if filepath == None:
            filepath = filedialog.asksaveasfilename(
                filetypes=(('Text files', '*.txt'), ('All files', '*.*')))
        try:
            with open(filepath, 'wb') as f:
                text = self.editor.get(1.0, "end-1c")
                f.write(bytes(text, 'UTF-8'))
                self.editor.edit_modified(False)
                self.file_path = filepath
                self.set_title()
                return "saved"
        except FileNotFoundError:
            return "cancelled"

    def file_quit(self, event=None):
        result = self.save_if_modified()
        if result != None:
            self.root.destroy() #sys.exit(0)

    def set_title(self, event=None):
        if self.file_path != None:
            title = os.path.basename(self.file_path)
        else:
            title = "Untitled"
        self.root.title(title + " - " + self.TITLE)

    def undo(self, event=None):
        try:
            self.editor.edit_undo()
        except TclError:
            pass

    def redo(self, event=None):
        try:
            self.editor.edit_redo()
        except TclError:
            pass

    def selectall(self, event=None):
        try:
            self.editor.tag_add(SEL, "1.0", END)
            self.editor.mark_set(INSERT, "1.0")
            self.editor.see(INSERT)
            return 'break'
        except TclError:
            pass
        return None

    def main(self, event=None):
        self.editor.bind("<"+self.commande+"-o>", self.file_open)
        self.editor.bind("<"+self.commande+"-s>", self.file_save)
        self.editor.bind("<Shift-"+self.commande+"-z>", self.redo)
        self.editor.bind("<"+self.commande+"-z>", self.undo)
        self.editor.bind("<Shift-"+self.commande+"-c>", self.convert)
        self.editor.bind("<"+self.commande+"-d>", self.remove_specs_chars)
        self.editor.bind("<"+self.commande+"-a>", self.selectall)

    def convert(self, event=None, filepath=None):
        arbo = self.convert_text(str(self.editor.get(1.0, "end-1c")))
        if not filepath or not self.file_path:
            filepath = filedialog.asksaveasfilename(
                filetypes=(('Text files', '*.txt'), ('All files', '*.*')))
        try:
            with open(filepath, 'wb') as f:
                text = str(arbo)
                f.write(bytes(text, 'UTF-8'))
        except FileNotFoundError:
            pass

    def remove_specs_chars(self, event=None):
        cleaned_text = self.remove_specs_chars_text(str(self.editor.get(1.0,
            "end-1c")))
        self.editor.delete(1.0, "end")
        self.editor.insert(1.0, cleaned_text)
        self.editor.edit_modified(False)

    def set_casio_chars_CG20(self, text):
        casio_chars_CG20 = {
            "_A" : "_@E741_",
            "_B" : "_@E742_",
            "_C" : "_@E743_",
            "_D" : "_@E744_",
            "_E" : "_@E745_",
            "_F" : "_@E746_",
            "_G" : "_@E747_",
            "_H" : "_@E748_",
            "_I" : "_@E749_",
            "_J" : "_@E74A_",
            "_K" : "_@E74B_",
            "_L" : "_@E74C_",
            "_M" : "_@E74D_",
            "_N" : "_@E74E_",
            "_O" : "_@E74F_",
            "_P" : "_@E750_",
            "_Q" : "_@E751_",
            "_R" : "_@E752_",
            "_S" : "_@E753_",
            "_T" : "_@E754_",
            "_U" : "_@E755_",
            "_V" : "_@E756_",
            "_W" : "_@E757_",
            "_X" : "_@E758_",
            "_Y" : "_@E759_",
            "_Z" : "_@E75A_",

            "_a" : "_@E761_",
            "_b" : "_@E762_",
            "_c" : "_@E763_",
            "_d" : "_@E764_",
            "_e" : "_@E765_",
            "_f" : "_@E766_",
            "_g" : "_@E767_",
            "_h" : "_@E768_",
            "_i" : "_@E769_",
            "_j" : "_@E76A_",
            "_k" : "_@E76B_",
            "_l" : "_@E76C_",
            "_m" : "_@E76D_",
            "_n" : "_@E76E_",
            "_o" : "_@E76F_",
            "_p" : "_@E770_",
            "_q" : "_@E771_",
            "_r" : "_@E772_",
            "_s" : "_@E773_",
            "_t" : "_@E774_",
            "_u" : "_@E775_",
            "_v" : "_@E776_",
            "_w" : "_@E777_",
            "_x" : "_@E778_",
            "_y" : "_@E779_",
            "_z" : "_@E77A_",

            "_0" : "_#E5D0_",
            "_1" : "_#E5D1_",
            "_2" : "_#E5D2_",
            "_3" : "_#E5D3_",
            "_4" : "_#E5D4_",
            "_5" : "_#E5D5_",
            "_6" : "_#E5D6_",
            "_7" : "_#E5D7_",
            "_8" : "_#E5D8_",
            "_9" : "_#E5D9_",
            "_+" : "_#E5DB_",
            "_-" : "_#E5DC_",

            "^0" : "_#E5C0_",
            "^1" : "_#E5C1_",
            "^2" : "_#E5C2_",
            "^3" : "_#E5C3_",
            "^4" : "_#E5C4_",
            "^5" : "_#E5C5_",
            "^6" : "_#E5C6_",
            "^7" : "_#E5C7_",
            "^8" : "_#E5C8_",
            "^9" : "_#E5C9_",
            "^+" : "_#E5CB_",
            "^-" : "_#E5CC_",

            "√" : "_Sqrt_",
            "∑" : "_#E551_",
            "^x" :  "_#E5DD_"
        }
        for char, conv in casio_chars_CG20.items():
            text = text.replace(char,conv)
        return text

    def convert_line(self, text):
        #Specials caracters
        char_to_conv = {
            'é':'e',
            'é':'e',
            'è':'e',
            'ê':'e',
            'ë':'e',
            'ô':'o',
            'û':'u',
            'à':'a',
            'î':'i',
            'ï':'i',
            'æ':'ae',
            'œ':'oe',
            '\'':' ',
            '’':' ',
            '‘':' ',
            '"':' ',
            '”':' ',
            '“':' ',
            '–':'-',
            '’':' '
        }
        for char, conv in char_to_conv.items():
            text = text.replace(char,conv)
        return text

    def remove_specs_chars_text(self, text):
        cleaned_text = ""
        for line in text.splitlines():
            if line:
                line_cleaned = self.convert_line(line)
                template_title = re.match(r'^(##).*', line_cleaned)
                if template_title:
                    cleaned_text += line_cleaned[:MAIN_TITLE_MAX_LEN] + "\n"
                    continue
                template_title = re.match(r'^(--|//).*', line_cleaned)
                if template_title:
                    cleaned_text += line_cleaned[:MENU_TITLE_MAX_LEN] + "\n"
                    continue
                #Mode brute
                #Chaque ligne est coupée tous les 30 caractères,
                #même les mots sont coupés.
                #while len(line_cleaned) > TEXT_MAX_LEN:
                #    cleaned_text += line_cleaned[:TEXT_MAX_LEN] + "\n"
                #    line_cleaned = line_cleaned[TEXT_MAX_LEN:]
                #cleaned_text += line_cleaned + "\n"
                words = iter(line_cleaned.split())
                current = next(words)
                for word in words:
                    while len(current) > TEXT_MAX_LEN:
                        cleaned_text+=current[:TEXT_MAX_LEN]+"\n"
                        current = current[TEXT_MAX_LEN:]
                    if len(current) + 1 + len(word) > TEXT_MAX_LEN:
                        cleaned_text+=current+"\n"
                        current = word
                    else:
                        current += " " + word
                cleaned_text+=current+"\n"
        return cleaned_text

    def convert_text(self, text):
        cleaned_text = ""
        for line in text.splitlines():
            if line:
                line_cleaned = self.convert_line(line)
                if self.remp_sp_chrs.get() == True:
                    cleaned_text += self.set_casio_chars_CG20(line_cleaned) + "\n"
                else:
                    cleaned_text += line_cleaned + "\n"
        lined_text = cleaned_text.split("\n")
        return self.fill_template(self.get_arbo(lined_text))

    def fill_template(self, data):
        templateLoader = jinja2.FileSystemLoader(searchpath="./")
        templateEnv = jinja2.Environment(loader=templateLoader)
        casio_type = self.listtype.get(ACTIVE)
        if casio_type == G35 :
            template = templateEnv.get_template(TEMPLATE_FILE_G35)
            outputText = template.render(context = {
                "data" : data,
                "text_height" : TEXT_HEIGHT_G35,
                "text_jump" : TEXT_JUMP_G35})
        else:
            template = templateEnv.get_template(TEMPLATE_FILE_CG20)
            outputText = template.render(context = {
                "data" : data,
                "text_height" : TEXT_HEIGHT_CG20,
                "text_jump" : TEXT_JUMP_CG20})
        text_list_len30 = list()
        return os.linesep.join([s for s in outputText.splitlines() if s])

    def get_arbo_example(self):
        return {
            "0" : {
                    "title" : "titre menu principal",
                    "sub_modules" : {
                        "A" : {
                            "title" : "titre menu 1",
                            "sub_modules" : None,
                            "text" : ["Hello", "ca va"]
                        },
                        "B" : {
                            "title" : "titre menu 2",
                            "sub_modules" : {
                                "C" : {
                                    "title" : "titre sous menu 2-1",
                                    "sub_modules" : None,
                                    "text" : ["Blablabla", "Texte super long \
                                        qui doit absolument être coupé pour \
                                        être prit en compte"]
                                },
                                "D" : {
                                    "title" : "titre sous menu 2-2",
                                    "sub_modules" : None,
                                    "text" : ["Blablabla", "Blablabla",
                                        "Blablabla", "Blablabla"]
                                },
                            },
                            "text" : None
                        },
                    },
                    "text" : None
            }
        }

    def get_arbo(self, lined_text):
        available_labels = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")
        arbor = {
            "0" : {
                    "title" : "Choix",
                    "sub_modules" : None,
                    "text" : list()
            }
        }
        content = ""
        curent_menu = None
        curent_smenu = None
        for line in lined_text:
            template_title = re.match(r'^##(?P<content>.*)', line)
            if template_title:
                arbor["0"]["title"] = template_title.group("content")
                continue
            template_menu = re.match(r'^//(?P<content>.*)', line)
            if template_menu:
                content = template_menu.group("content")
                curent_menu = available_labels.pop()
                curent_smenu = None
                if not arbor["0"]["sub_modules"]:
                    arbor["0"]["sub_modules"] = OrderedDict()
                arbor["0"]["sub_modules"][curent_menu] = {
                    "title" : content,
                    "sub_modules" : None,
                    "text" : list()
                }
                continue
            template_sub_menu = re.match(r'^--(?P<content>.*)', line)
            if template_sub_menu:
                content = template_sub_menu.group("content")
                curent_smenu = available_labels.pop()
                if not arbor["0"]["sub_modules"][curent_menu]["sub_modules"]:
                    arbor["0"]["sub_modules"][curent_menu]["sub_modules"] = \
                        OrderedDict()
                arbor["0"]["sub_modules"][curent_menu]["sub_modules"] \
                [curent_smenu] = {
                    "title" : content,
                    "sub_modules" : None,
                    "text" : list()
                }
                continue
            template_line = re.match(r'(?P<content>.*)', line)
            if template_line:
                content = template_line.group("content")
                if curent_smenu and curent_menu:
                    arbor["0"]["sub_modules"][curent_menu]["sub_modules"] \
                        [curent_smenu]["text"].append(content)
                elif curent_menu:
                    arbor["0"]["sub_modules"][curent_menu]["text"] \
                        .append(content)
                else:
                    arbor["0"]["text"].append(content)
        return arbor


if __name__ == "__main__":
    root = Tk()
    root.wm_state('zoomed')
    editor = Editor(root)
    editor.main()
    root.mainloop()
