{% set ns = namespace(i=0) %}
{% set text_jump_len = context["text_jump"]|length %}
ClrGraphÙ
AxesOffÙ
ViewWindow 1,127,0,1,63,0Ù
Lbl 0Ù
ClsÙ
{% if context["data"]["0"]["sub_modules"] %}
Menu "{{context["data"]["0"]["title"]}}",{% for key, item in context["data"]["0"]["sub_modules"].items() %}"{{ item["title"] }}",{{ key }}{% if not loop.last %},{% endif %}{% endfor %}Ù
{% for key, item in context["data"]["0"]["sub_modules"].items() %}
Lbl {{ key }}Ù
{% if item["sub_modules"] %}
Menu "{{item["title"]}}",{% for s_key, s_item in item["sub_modules"].items() %}"{{ s_item["title"] }}",{{ s_key }},{% endfor %}"exit",0Ù
{% for s_key, s_item in item["sub_modules"].items() %}
Lbl {{ s_key }}Ù
ClsÙ
{% set ns.i = 0 %}
{% for text in s_item["text"] %}
Text {{context["text_jump"][ns.i]}},{{context["text_height"]}},"{{ text }}"{% set ns.i = ns.i + 1 %}{% if ns.i >= text_jump_len %}Ø
Cls{% set ns.i = 0 %}{% endif %}{% if loop.last %}Disps{% else %}Ù{% endif %}
{% endfor %}
Goto {{ key }}Ù
{% endfor %}
{% else %}
ClsÙ
{% set ns.i = 0 %}
{% for text in item["text"] %}
Text {{context["text_jump"][ns.i]}},{{context["text_height"]}},"{{ text }}"{% set ns.i = ns.i + 1 %}{% if ns.i >= text_jump_len %}Ø
Cls{% set ns.i = 0 %}{% endif %}{% if loop.last %}Disps{% else %}Ù{% endif %}
{% endfor %}
Goto 0Ù
{% endif %}
{% endfor %}
{% else %}
{% for text in context["data"]["0"]["text"] %}
Text {{context["text_jump"][ns.i]}},{{context["text_height"]}},"{{ text }}"{% set ns.i = ns.i + 1 %}{% if ns.i >= text_jump_len %}Ø
Cls{% set ns.i = 0 %}{% endif %}{% if loop.last %}Disps{% else %}Ù{% endif %}
{% endfor %}
{% endif %}
